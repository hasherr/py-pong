# Entities file for containing all the various entities that may exist inside of a pong game.

__author__ = 'Evan'

import pygame
from pygame import *


class Entity(object):
    def __init__(self):
        self.vector = []

    def render(self):None

    def update(self):None


class Ball(Entity):
    def __init__(self, screen_to_draw_to):
        self.screen = screen_to_draw_to
        self.vector = [screen_to_draw_to.get_width() / 2, screen_to_draw_to.get_height() / 2]
        self.xspeed = 0.081
        self.yspeed = 0.085
        self.rectangle = pygame.Rect(self.vector[0], self.vector[1], 20, 20)

    def render(self):
        pygame.draw.circle(self.screen, (2, 70, 240), (int(self.vector[0]), int(self.vector[1])), 20)

    def update(self, paddle1, paddle2):
        self.__update_bounding_box()
        self.vector[0] += self.xspeed
        self.vector[1] += self.yspeed
        self.__handle_wall_collision()
        self.check_for_reset(self.screen, paddle1, paddle2)

    def bounce_back(self, direction):
        self.xspeed = -self.xspeed
        # self.yspeed = -self.yspeed
        self.vector[0] += direction

    def check_for_reset(self, screen_to_draw_to, paddle1, paddle2):
        if self.rectangle.right > screen_to_draw_to.get_width():
            self.__reset(screen_to_draw_to)
            paddle1.update_score()
        elif self.rectangle.left < 0:
            self.__reset(screen_to_draw_to)
            paddle2.update_score()

    def __reset(self, screen_to_draw_to):
            self.vector = [screen_to_draw_to.get_width() / 2, screen_to_draw_to.get_height() / 2]
            self.xspeed = 0.081
            self.yspeed = 0.085

    def __update_bounding_box(self):
        self.rectangle.x = self.vector[0]
        self.rectangle.y = self.vector[1]

    def __handle_wall_collision(self):
        if self.vector[1] > self.screen.get_height() or self.vector[1] < 0:
            self.yspeed = -self.yspeed


class Paddle(Entity):
    def __init__(self, screen_to_draw_to, vector):
        self.screen = screen_to_draw_to
        self.vector = vector
        self.speed = 0
        self.score = 0
        self.rectangle = pygame.Rect(vector[0], vector[1], 30, 90)
        pygame.font.init()
        self.score_font = pygame.font.SysFont("monospace", 50)

    def render(self, font_pos):
        self.rectangle.x = self.vector[0]
        self.rectangle.y = self.vector[1]
        pygame.draw.rect(self.screen, (2, 240, 70), self.rectangle)
        label = self.score_font.render(str(self.score), 1, (0, 0, 0))
        self.screen.blit(label, font_pos)

    def __handle_boundary_collision(self):
        if self.vector[1] < 0:
            self.vector[1] = 1
        elif self.vector[1] + 90 > self.screen.get_height():
            self.vector[1] = self.screen.get_height() - 90

    def update(self):
        self.__handle_boundary_collision()
        self.vector[1] += self.speed

    def update_score(self):
        self.score += 1

    def set_speed(self, value):
        self.speed = value

    def check_for_collision(self, ball, side):
        if self.rectangle.colliderect(ball.rectangle):
            if side == "left":
                ball.bounce_back(1)
            else:
                ball.bounce_back(-1)