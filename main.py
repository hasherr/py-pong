# Py-Pong is a little pet project of mine used for the intention of learning about Pygame.

__author__ = 'Evan'

import pygame, entities, sys
from pygame.locals import *
from entities import *
import random

screen = pygame.display.set_mode((500, 350))
ball = Ball(screen)
paddle1 = Paddle(screen, [0, screen.get_height() / 2 - 45])
paddle2 = Paddle(screen, [screen.get_width() - 30, screen.get_height() / 2 - 45])

def main():
    pygame.init()
    game_loop(screen, (255, 255, 255))

def game_loop(screen_to_draw_on, background_color):
    while True:
        check_for_game_end()
        screen_to_draw_on.fill(background_color)
        handle_entities()
        handle_input()
        pygame.display.flip()

def handle_entities():
    ball.render()
    ball.update(paddle1, paddle2)
    paddle1.render((screen.get_width() / 4, 175))
    paddle1.update()

    paddle2.render((375, 175))
    paddle2.update()
    paddle1.check_for_collision(ball, "left")
    paddle2.check_for_collision(ball, "right")

def handle_input():
        key = pygame.key.get_pressed()
        if key[pygame.K_s]:
            paddle1.set_speed(0.2)
        if key[pygame.K_w]:
            paddle1.set_speed(-0.2)
        if key[pygame.K_UP]:
            paddle2.set_speed(-0.2)
        if key[pygame.K_DOWN]:
            paddle2.set_speed(0.2)

        if not key[pygame.K_w] and not key[pygame.K_s]:
            paddle1.set_speed(0.0)
        if not key[pygame.K_DOWN] and not key[pygame.K_UP]:
            paddle2.set_speed(0.0)


def check_for_game_end():
    for event1 in pygame.event.get():
            if event1.type == QUIT:
                sys.exit()

main() # Start the game.